# bidirectional hash

A simple bidrectional hash implementation based on this 
[Psuedo Encrypt](https://wiki.postgresql.org/wiki/Pseudo_encrypt) algorithm, which is a Feistel network, with support up
to 128 bit numbers.

## Usage

```shell
cargo add bidirectional_hash
```

```rust
use bidirectional_hash::hash;

fn main() {
    let x: u64 = 123456789;
    let h = hash(x.into()); // Input::U64(x)
    assert_eq!(hash(h), x);
}
```




